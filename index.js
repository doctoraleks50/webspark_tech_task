function minWalk(gridList, startX, startY, endX, endY) {
    let counter = 0;
    let next;
    function createMap(gridList) {
        let gridMap = [];
        gridList.forEach((row, rowNumber) => {
            let cols = row.split('');
            cols.forEach((col, colNumber) => {
                gridMap.push(
                    {
                        x: rowNumber,
                        y: colNumber,
                        blocked: (col === 'X')
                    });
            });
        });
        return gridMap;
    };
    const gridMap = createMap(gridList);
    function nextStep(startX, startY, endX, endY) {
        let x = (startX > endX) ? startX - 1 : (startX < endX) ? startX + 1 : startX;
        let y = (startY > endY) ? startY - 1 : (startY < endY) ? startY + 1 : startY;

        next = gridMap.find(cell => cell.x === x && cell.y === y);

        if(next.blocked === true) {
            next = gridMap.find(cell => cell.x === startX && cell.y === y);
        }
        if(next.blocked === true) {
            next = gridMap.find(cell => cell.x === x && cell.y === startY);
        }
        return next
    };
    function findNext() {

        next = nextStep(startX, startY, endX, endY);
        if(next.blocked === false) {
            startY = next.y;
            startX = next.x;
            counter++;
        }
        if(startX === endX && startY === endY) {
            return ;
        } else {
            findNext();
        }
    }
    function runAndCount() {
        findNext();
        return counter
    };
    return runAndCount();
}

const result = minWalk(
    [
        '.X.',
        '.X.',
        '...',
    ],
    2, 1,
    0, 2
);

console.log(`${result} steps`);